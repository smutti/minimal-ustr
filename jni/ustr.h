/*
 * ustr.h
 *
 *  Created on: Apr 4, 2014
 *      Author: mutti
 */

#ifndef USTR_H_
#define USTR_H_

#include "ustr-private.h"

extern inline struct Ustr *ustr_dup_cstr(const char *cstr) {
	return (ustr_dup_buf(cstr, strlen(cstr)));
}

static inline struct Ustr *ustr_dup_buf(const void *data, size_t len) {
	return (ustr_dupx_buf(USTR__DUPX_DEF, data, len));
}

static inline struct Ustr *ustr_dupx_buf(size_t sz, size_t rb, int exact,
		int emem, const void *data, size_t len) {
	return (ustrp__dupx_buf(0, sz, rb, exact, emem, data, len));
}

static inline struct Ustr *ustrp__dupx_buf(struct Ustr_pool *p, size_t sz,
		size_t rbytes, int exact, int emem, const void *data, size_t len) {
	struct Ustr *s1 = ustrp__dupx_undef(p, sz, rbytes, exact, emem, len);

	if (!s1)
		return (USTR_NULL);

	ustr__memcpy(s1, 0, data, len);

//	USTR_ASSERT(ustrp__assert_valid(!!p, s1));
	return (s1);
}

/* ---------------- allocations ---------------- */
/* NOTE: This is one of the two main "allocation" functions --
 * this is the only thing that calls MALLOC. */
static inline struct Ustr *ustrp__dupx_undef(struct Ustr_pool *p, size_t sz,
		size_t rbytes, int exact, int emem, size_t len) {
	struct Ustr *ret = USTR_NULL;
	struct Ustr *chk = USTR_NULL;
	size_t rsz = 0;

//	USTR_ASSERT(
//			(rbytes == 0) || (rbytes == 1) || (rbytes == 2) || (rbytes == 4)
//					|| (USTR_CONF_HAVE_64bit_SIZE_MAX && (rbytes == 8)));
//	USTR_ASSERT(exact == !!exact);
//	USTR_ASSERT(emem == !!emem);

	if (!len && ustr__dupx_cmp_eq(sz, rbytes, exact, emem, USTR__DUPX_DEF))
		return (USTR("")); /* We don't go to all the trouble ustr_del() does.
		 * Which is probably better overall. */

	if (!(rsz = ustr_init_size(sz, rbytes, exact, len)))
		return (USTR_NULL);

	if (p)
		ret = p->pool_sys_malloc(p, rsz);
	else
		ret = USTR_CONF_MALLOC(rsz);

	if (!ret) {
		errno = USTR__ENOMEM;
		return (USTR_NULL);
	}

	chk = ustr_init_alloc(ret, rsz, sz ? rsz : 0, rbytes, exact, emem, len);
//	USTR_ASSERT(chk);
//
//	USTR_ASSERT(ustrp__assert_valid(!!p, ret));
	return (ret);
}

static inline
int ustr__dupx_cmp_eq(size_t sz1, size_t rb1, int x1, int emem1, size_t sz2,
		size_t rb2, int x2, int emem2) {
	if ((!x1 != !x2) || (!emem1 != !emem2))
		return (USTR_FALSE);

	if (sz1) {
		if (rb1 < 2)
			rb1 = 2;
	} else if (rb1 > 4)
		sz1 = 1;

	if (sz2) {
		if (rb2 < 2)
			rb2 = 2;
	} else if (rb2 > 4)
		sz2 = 1;

	return ((!sz1 == !sz2) && (rb1 == rb2));
}

/* ---------------- init - helpers so others can make Ustr's ---------------- */
static inline size_t ustr_init_size(size_t sz, size_t rbytes, int exact,
		size_t len) {
	size_t oh = 0;
	size_t rsz = sz ? sz : len;
	size_t lbytes = 0;

//  USTR_ASSERT_RET((rbytes == 0) ||
//                  (rbytes == 1) || (rbytes == 2) || (rbytes == 4) ||
//                  (USTR_CONF_HAVE_64bit_SIZE_MAX && (rbytes == 8)), 0);

	do {
		size_t sbytes = 0;

		lbytes = ustr__nb(rsz);
		if (!sz && ((lbytes == 8) || (rbytes == 8)))
			sz = 1;

//    USTR_ASSERT(    (lbytes == 1) || (lbytes == 2) || (lbytes == 4) ||
//                    (USTR_CONF_HAVE_64bit_SIZE_MAX && (lbytes == 8)));

		if (sz) {
			if (rbytes <= 1)
				rbytes = 2;
			if (lbytes <= 1)
				lbytes = 2;
			sbytes = lbytes;
		}

		oh = 1 + rbytes + lbytes + sbytes + sizeof(USTR_END_ALOCDx);
		rsz = oh + len;

		if (rsz < len) {
			errno = USTR__EINVAL;
			return (0);
		}

//    USTR_ASSERT((lbytes <= ustr__nb(rsz)) ||
//                ((lbytes == 2) && sz && (ustr__nb(rsz) == 1)));
	} while (lbytes < ustr__nb(rsz));

	if (exact)
		return (rsz);

	return (ustr__ns(rsz));
}

static inline size_t ustr__nb(size_t num) {
	//USTR_ASSERT((num <= 0xFFFFFFFF) || USTR_CONF_HAVE_64bit_SIZE_MAX);

	if (num > 0xFFFFFFFF)
		return (8);
	if (num > 0xFFFF)
		return (4);
	if (num > 0xFF)
		return (2);
	else
		return (1);
}

static inline size_t ustr__ns(size_t num) {
	size_t min_sz = 4;

	if (num > ((USTR__SIZE_MAX / 4) * 3))
		return (USTR__SIZE_MAX);

	/* *2 is too much, we end up wasting a lot of RAM. So we do *1.5. */
	while (min_sz < num) {
		size_t adder = min_sz / 2;

		min_sz += adder;
		if (min_sz >= num)
			break;
		min_sz += adder;
	}

	return (min_sz);
}

static inline struct Ustr *ustr_init_alloc(void *data, size_t rsz, size_t sz,
		size_t rbytes, int exact, int emem, size_t len) {
	static const unsigned char map_big_pow2[9] = { -1, -1, 0, -1, 1, -1, -1, -1,
			2 };
	static const unsigned char map_pow2[5] = { 0, 1, 2, -1, 3 };
	struct Ustr *ret = data;
	int nexact = !exact;
	int sized = 0;
	size_t lbytes = 0;
	size_t sbytes = 0;
	size_t oh = 0;
	const size_t eos_len = sizeof(USTR_END_ALOCDx);

//  USTR_ASSERT_RET((rbytes == 0) ||
//                  (rbytes == 1) || (rbytes == 2) || (rbytes == 4) ||
//                  (USTR_CONF_HAVE_64bit_SIZE_MAX && (rbytes == 8)), USTR_NULL);
//  USTR_ASSERT(data);
//  USTR_ASSERT(exact == !!exact);
//  USTR_ASSERT(emem  == !!emem);
//  USTR_ASSERT(!sz || (sz == rsz));
//  USTR_ASSERT_RET(!sz || (sz > len), USTR_NULL);

	if (!sz && (rbytes == 8))
		sz = rsz; /* whee... */

	lbytes = ustr__nb(sz ? sz : len);
	if (!sz && (lbytes == 8))
		sz = rsz; /* whee... */

//  USTR_ASSERT(lbytes == ustr__nb(sz ? sz : len));
//
//  USTR_ASSERT(    (lbytes == 1) || (lbytes == 2) || (lbytes == 4) ||
//                  (USTR_CONF_HAVE_64bit_SIZE_MAX && (lbytes == 8)));

	if (sz) {
		if (sz < (1 + 2 + 2 + 1))
			goto val_inval;

		if (rbytes <= 1)
			rbytes = 2;
		if (lbytes <= 1)
			lbytes = 2;
		sbytes = lbytes;
	}
	oh = 1 + rbytes + lbytes + sbytes + eos_len;

	if (rsz < (oh + len))
		goto val_inval;

	if (sz)
		sized = USTR__BIT_HAS_SZ;
	if (nexact)
		nexact = USTR__BIT_NEXACT;
	if (emem)
		emem = USTR__BIT_ENOMEM;

	ret->data[0] = USTR__BIT_ALLOCD | sized | nexact | emem;
	if (sz)
		ret->data[0] |= (map_big_pow2[rbytes] << 2) | map_big_pow2[lbytes];
	else
		ret->data[0] |= (map_pow2[rbytes] << 2) | map_pow2[lbytes];

	ustr__terminate(ret->data, USTR_TRUE, (oh - eos_len) + len);

	if (sz)
		ustr__sz_set(ret, sz);
	ustr__len_set(ret, len);
	ustr__ref_set(ret, 1);

//  USTR_ASSERT(ustr_assert_valid(ret));
//  USTR_ASSERT( ustr_alloc(ret));
//  USTR_ASSERT(!ustr_fixed(ret));
//  USTR_ASSERT(!ustr_ro(ret));
//  USTR_ASSERT( ustr_enomem(ret) == !!emem);
//  USTR_ASSERT( ustr_exact(ret)  == exact);
//  USTR_ASSERT(!ustr_shared(ret));
//  USTR_ASSERT( ustr_owner(ret));

	return (ret);

	val_inval:
	errno = USTR__EINVAL;
	return (USTR_NULL);
}

/* NIL terminate -- with possible end marker */
static inline void ustr__terminate(unsigned char *ptr, int alloc, size_t len) {
	if (sizeof(USTR_END_ALOCDx) == 1)
		ptr[len] = 0;
	else if (alloc)
		memcpy(ptr + len, USTR_END_ALOCDx, sizeof(USTR_END_ALOCDx));
	else
		memcpy(ptr + len, USTR_END_FIXEDx, sizeof(USTR_END_FIXEDx));
}

static inline void ustr__len_set(struct Ustr *s1, size_t len) { /* can only validate after the right len is in place */
	unsigned char *data = s1->data;

//  USTR_ASSERT(!ustr_ro(s1));
	ustr__embed_val_set(data + 1 + USTR__REF_LEN(s1), USTR__LEN_LEN(s1), len);
//  USTR_ASSERT(ustr_assert_valid(s1));
}

static inline void ustr__embed_val_set(unsigned char *data, size_t len,
		size_t val) {
	switch (len) {
	case 4:
		data[3] = (val >> 24) & 0xFF;
		data[2] = (val >> 16) & 0xFF;
	case 2:
		data[1] = (val >> 8) & 0xFF;
	case 1:
		data[0] = (val >> 0) & 0xFF;

//		USTR_ASSERT_NO_SWITCH_DEF("Val. length bad for ustr__embed_val_set()");
	}
}

/* no warn here, because most callers already know it's not going to fail */
static inline int ustr__ref_set(struct Ustr *s1, size_t ref) {
	size_t len = 0;

//	USTR_ASSERT(ustr_assert_valid(s1));
//	USTR_ASSERT(ustr_alloc(s1));

	if (!(len = USTR__REF_LEN(s1)))
		return (USTR_FALSE);

	ustr__embed_val_set(s1->data + 1, len, ref);

//	USTR_ASSERT(ustr_assert_valid(s1));

	return (USTR_TRUE);
}

static inline void ustr__memcpy(struct Ustr *s1, size_t off, const void *ptr,
		size_t len) { /* can't call ustr_wstr() if len == 0, as it might be RO */
	if (!len)
		return;
	memcpy(ustr_wstr(s1) + off, ptr, len);
}

static inline char *ustr_wstr(struct Ustr *s1) { /* NOTE: Not EI/II so we can call ustr_assert_valid() here. */
	unsigned char *data = s1->data;
	size_t lenn = 0;

//	USTR_ASSERT(ustr_assert_valid(s1));
//	USTR_ASSERT_RET(!ustr_ro(s1), 0);

	lenn = USTR__LEN_LEN(s1);
	if (ustr_sized(s1))
		lenn *= 2;

	return ((char *) (data + 1 + USTR__REF_LEN(s1) + lenn));
}

#endif /* USTR_H_ */
