LOCAL_PATH:= $(call my-dir)

ustr_src_files := \
	ustr.c
	
ustr_includes := \
	$(LOCAL_PATH)/ 
	
common_cflags := \
	-Wall -W -Wundef \
	-Wshadow -Wmissing-noreturn \
	-Wmissing-format-attribute -DDARWIN

ifeq ($(HOST_OS), darwin)
common_cflags += -DDARWIN
endif

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(ustr_src_files)
LOCAL_C_INCLUDES := $(ustr_includes) 
LOCAL_CFLAGS := $(common_cflags)
LOCAL_MODULE:= libustr
LOCAL_MODULE_TAGS := eng
include $(BUILD_STATIC_LIBRARY)