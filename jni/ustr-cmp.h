/*
 * ustr-cmp.h
 *
 *  Created on: Apr 4, 2014
 *      Author: mutti
 */

#ifndef USTR_CMP_H_
#define USTR_CMP_H_

#include "ustr-private.h"

extern inline int ustr_cmp_case_suffix_eq(const struct Ustr *s1,
		const struct Ustr *s2) {
//	USTR_ASSERT(ustr_assert_valid(s1) && ustr_assert_valid(s2));

	if (s1 == s2)
		return (USTR_TRUE);

	return (ustr_cmp_case_suffix_buf_eq(s1, ustr_cstr(s2), ustr_len(s2)));
}

static inline int ustr_cmp_case_suffix_buf_eq(const struct Ustr *s1,
		const void *buf, size_t len2) {
	size_t len1 = 0;

//  USTR_ASSERT(ustr_assert_valid(s1) && buf);

	len1 = ustr_len(s1);
	if (len1 < len2)
		return (USTR_FALSE);

	return (!ustr__memcasecmp(ustr_cstr(s1) + (len1 - len2), buf, len2));
}

static inline
int ustr__memcasecmp(const void *passed_s1, const void *passed_s2, size_t len) {
	const unsigned char *s1 = passed_s1;
	const unsigned char *s2 = passed_s2;

	while (len) {
		unsigned char c1 = *s1;
		unsigned char c2 = *s2;

		if ((c1 >= 0x61) && (c1 <= 0x7a))
			c1 ^= 0x20;
		if ((c2 >= 0x61) && (c2 <= 0x7a))
			c2 ^= 0x20;

		if (c1 != c2)
			return (c1 - c2);

		++s1;
		++s2;
		--len;
	}

	return (0);
}

/* NOTE: This is the main "deallocation" function (apart from plain free) --
 * this is one of only three things that removes data via. REALLOC.
 * Others are in ustr-set.h */
static inline
int ustrp__del(struct Ustr_pool *p, struct Ustr **ps1, size_t len) {
	struct Ustr *s1 = USTR_NULL;
	struct Ustr *ret = USTR_NULL;
	size_t sz = 0;
	size_t oh = 0;
	size_t osz = 0;
	size_t nsz = 0;
	size_t clen = 0;
	size_t nlen = 0;
	int alloc = USTR_FALSE;

//	USTR_ASSERT(ps1 && ustrp__assert_valid(!!p, *ps1));

	if (!len)
		return (USTR_TRUE);

	s1 = *ps1;
	clen = ustr_len(s1);
	/* under certain conditions, we can just return "" as it's much more efficient
	 * and we don't get anything with not doing it. */
	if (!(nlen = clen - len) && /* we are deleting everything */
	!(ustr_fixed(*ps1) || /* NOT in "free" space, or */
	(ustr_sized(*ps1) && ustr_owner(*ps1))) && /* sized with one ref. */
	ustr__dupx_cmp_eq(USTR__DUPX_DEF, USTR__DUPX_FROM(s1))) /* def. config. */
	{
		ustrp__sc_free2(p, ps1, USTR(""));
		return (USTR_TRUE);
	}

	if (nlen > clen) /* underflow */
		return (USTR_FALSE);

	if (ustr__rw_mod(s1, nlen, &sz, &oh, &osz, &nsz, &alloc)) {
		size_t eos_len = sizeof(USTR_END_ALOCDx);

		if (alloc) { /* ignore errors? -- can they happen on truncate? */
			int emem = ustr_enomem(*ps1);

			USTR_ASSERT(nsz < osz);
			USTR_ASSERT(!sz);

			if (!ustrp__rw_realloc(p, ps1, USTR_FALSE, osz, nsz)) {
				if (!p) {
					ustr_assert(USTR_CNTL_MALLOC_CHECK_MEM_SZ(*ps1, osz));
					USTR__CNTL_MALLOC_CHECK_FIXUP_REALLOC(*ps1, nsz);
					ustr_assert(USTR_CNTL_MALLOC_CHECK_MEM_SZ(*ps1, nsz));
				}

				if (!emem)
					ustr_setf_enomem_clr(*ps1);
			}
		}

		ustr__terminate((*ps1)->data, ustr_alloc(*ps1), (oh - eos_len) + nlen);
		ustr__len_set(*ps1, nlen);

//		USTR_ASSERT(ustrp__assert_valid(!!p, *ps1));
		return (USTR_TRUE);
	}

//	USTR_ASSERT(!ustr_limited(s1));

	if (!(ret = ustrp__dupx_undef(p, USTR__DUPX_FROM(s1), nlen))) {
		ustr_setf_enomem_err(s1);
		return (USTR_FALSE);
	}

	ustr__memcpy(ret, 0, ustr_cstr(s1), nlen);
	ustrp__sc_free2(p, ps1, ret);

//	USTR_ASSERT(ustrp__assert_valid(!!p, *ps1));
	return (USTR_TRUE);
}
static inline int ustr_del(struct Ustr **ps1, size_t len) {
	return (ustrp__del(0, ps1, len));
}

static inline void ustrp__free(struct Ustr_pool *p, struct Ustr *s1) {
	if (!s1)
		return;

//	USTR_ASSERT(ustrp__assert_valid(!!p, s1));

	if (!ustr__ref_del(s1)) {
		if (p)
			p->pool_sys_free(p, s1);
		else
			USTR_CONF_FREE(s1);
	}
}

static inline void ustr_free(struct Ustr *s1) {
	return (ustrp__free(0, s1));
}
static inline void ustrp_free(struct Ustr_pool *p, struct Ustrp *s1) {
	return (ustrp__free(p, &s1->s));
}

/* shortcut -- needs to be here, as lots of things calls this */
static inline
void ustrp__sc_free2(struct Ustr_pool *p, struct Ustr **ps1, struct Ustr *s2) {
//	USTR_ASSERT(ps1);
//	USTR_ASSERT(ustrp__assert_valid(!!p, s2)); /* don't pass NULL */

	ustrp__free(p, *ps1);
	*ps1 = s2;
}

#endif /* USTR_CMP_H_ */
