/*
 * ustr-private.h
 *
 *  Created on: Apr 4, 2014
 *      Author: mutti
 */

#ifndef USTR_PRIVATE_H_
#define USTR_PRIVATE_H_

#define USTR_CONF_II_PROTO extern inline
#define USTR_CONF_I_PROTO static inline

#define USTR__DUPX_DEF 0
#define USTR_FALSE 0
#define USTR_TRUE 1

struct Ustrp { /* This is a type for the "pool" allocated Ustr it is a seperate type as a
 * way to catch errors at compile time with static typing. You might think
 * we'd define Ustrp to be identical to Ustr, and just cast ... however
 * I'm 99% sure that aliasing rules screw us over if we do that. */
	struct Ustr s;
};

#include <errno.h>
# define USTR__ENOMEM ENOMEM
#define USTR__EINVAL EINVAL

#define USTR_NULL ((struct Ustr *) 0)
#define USTR(x)  ((struct Ustr  *) x)
#define USTRP(x) ((struct Ustrp *) x)

#define USTR_CONF_REALLOC(x, y) realloc((x), (y))
#define USTR_CONF_FREE(x)       free(x)
#define USTR_CONF_MALLOC(x)     malloc(x)

#include <stdlib.h> /* size_t */
#include <string.h> /* strlen() / etc. */
#include <limits.h> /* CHAR_BIT */

# define USTR__SIZE_MAX ((size_t)-1)

#include <stdarg.h> /* va_list for *printf like functionality,
                     * needed globally now due to cntl and concat */
#include <stdio.h>  /* vsnprintf */

#define USTR_FALSE 0
#define USTR_TRUE  1
#define USTR_NULL  ((struct Ustr *) 0)
#define USTRP_NULL ((struct Ustrp *)0)
#define USTR_POOL_NULL ((struct Ustr_pool *)0)

# define USTR_END_CONSTx ""
# define USTR_END_ALOCDx ""
# define USTR_END_FIXEDx ""

#define USTR__REF_LEN(x)     ustr_xi__pow2(ustr_sized(x), (x)->data[0] >> 2)
#define USTR__LEN_LEN(x)     ustr_xi__pow2(ustr_sized(x), (x)->data[0])

/* Unused bit patterns for data[0]:
 *
 * 0bx0xx_x100 == lenn = 0
 * 0bx0xx_1x00
 * 0bx0x1_xx00
 * 0bx01x_xx00
 *              0bx1xx_xx00 => is used in sized
 * 0b10xx_xx00
 *
 * 0bx1xx_xx11 == 128bit size_t, yeh!
 * 0bx1xx_11xx
 *
 * 0b0xxx_x1xx == refn values for const/fixed
 * 0b0xxx_1xxx
 *
 * 0b00x1_xxxx == mem error for const
 * 0b001x_xxxx == not exact for const
 *
 */
#define USTR__BIT_ALLOCD (1<<7)
#define USTR__BIT_HAS_SZ (1<<6)
#define USTR__BIT_NEXACT (1<<5)
#define USTR__BIT_ENOMEM (1<<4)

#define USTR__BITS_RW      (USTR__BIT_ALLOCD | USTR__BIT_HAS_SZ)
#define USTR__BITS_FIXED                       USTR__BIT_HAS_SZ
#define USTR__BITS_RWLIM   (USTR__BIT_ALLOCD | USTR__BIT_HAS_SZ |       \
                            USTR__BIT_NEXACT)
#define USTR__BITS_LIMITED                     USTR__BIT_HAS_SZ

extern inline int ustr_alloc(const struct Ustr *s1) {
	return (!!(s1->data[0] & USTR__BIT_ALLOCD));
}
extern inline int ustr_exact(const struct Ustr *s1) {
	return (!(s1->data[0] & USTR__BIT_NEXACT));
}
extern inline int ustr_sized(const struct Ustr *s1) {
	return (!!(s1->data[0] & USTR__BIT_HAS_SZ));
}
extern inline int ustr_enomem(const struct Ustr *s1) {
	return (!!(s1->data[0] & USTR__BIT_ENOMEM));
}

extern inline size_t ustr_xi__pow2(int use_big, unsigned char len) { /* FIXME: for 2.0 or whatever make this something like:
 static const unsigned char map_pow2[4] = {0, 1, 2, sizeof(size_t)};
 */
	static const unsigned char map_big_pow2[4] = { 2, 4, 8, 16 };
	static const unsigned char map_pow2[4] = { 0, 1, 2, 4 };

	if (use_big)
		return (map_big_pow2[len & 0x03]);

	return (map_pow2[len & 0x03]);
}

/* As the saying goes:
 *   "you can tell a lot about the code from the data structure".
 *
 * So this is the non-magic version of the struct Ustr:
 *
 * struct Ustr
 * {
 *   const char *ptr;
 *   size_t reference_count;
 *   size_t length;
 *   size_t size;
 *
 *   unsigned char flag_is_readonly            : 1;
 *   unsigned char flag_is_fixed               : 1;
 *   unsigned char flag_exact_byte_allocations : 1;
 *   unsigned char flag_has_enomem_error       : 1;
 *   unsigned char flag_fail_on_alloc          : 1;
 * };
 *
 * ...however this "eats" memory for "small" strings, which is one reason given
 * why people don't use a real string ADT.
 *
 */
struct Ustr {
	unsigned char data[1];
/* 0b_wxyz_nnoo =>
 *
 *    w         = allocated
 *     x        = has size
 *      y       = round up allocations (off == exact allocations)
 *       z      = memory error
 *         nn   = refn
 *           oo = lenn
 *
 * Eg.
 *
 * 0b_0000_0000 ==
 *           "" ==
 * not allocated, [no size], [no round up allocs], no mem err, refn=0, lenn=0
 *
 * 0b1000_0001 ==
 *        0xA5 ==
 * allocated, no size, no round up allocs,         no mem err, refn=0, lenn=1
 *
 * 0b1010_0101 ==
 *        0xA5 ==
 * allocated, no size,    round up allocs,         no mem err, refn=1, lenn=1
 */
};

/*
 *    ================ English description of struct Ustr ================
 *
 *  This strucure is very magic, as an optimization "" is a valid "structure"
 * for an empty string, ustr_len("") == 0, ustr_size("") == 0,
 * ustr_cstr("") == "".
 *  This also works for other "static data strings" if they start with the first
 * four bits as 0 ... although for C const strings using this, the length has
 * to be manually defined correctly at compile time.
 *
 *  However it is also a "space efficient" String ADT, with a managed length,
 * _either_ with or without reference counts (so that ustr_dup() doesn't copy)
 * and with or without a stored size (so growing/shrinking a lot doesn't
 * cause lots of malloc/realloc usage). Note that for lengths of strings, or
 * reference counts that need >= 8 bytes of storage the Ustr _MUST_ contain
 * a stored size.
 *
 *  It can also track memory allocation errors over multiple function calls.
 *
 *  If the first byte == 0, then it's "" as above.
 *  Otherwise the first byte contains four flags, and 2 numbers (2 bits each).
 *
 *  The 1st flag says if the string is allocated. The 2nd flag says if it has a
 * stored size. The 3rd flag says if it isn't using "exact allocations", if it
 * is ustr_len() + ustr_overhead() == ustr_size_alloc(), otherwise there might
 * be room to grow without doing a memory allocation call[1]. The 4th flag says
 * if this Ustr has had a memory allocation error.
 * The two numbers are the mappings for the length of the reference count, and
 * the length of the length. Note that this mapping changes if the 2nd flag is
 * set.
 *
 * [1] There is an implied "size" of the string, based an how big it is. This
 * is a concession for speed efficiency, although it only grows at 1.5x
 * not 2x which is the norm. (again, to reduce memory waste). Again if this is
 * too much overhead, just use exact sized allocations.
 *
 *  Also NOTE if the 1st and 2nd flags are set, this means that the Ustr is in
 * fixed storge (like an auto array). Also if the 1st, 2nd and 3rd flags are
 * set, this means that the Ustr is limited, Ie. it's in fixed storge and
 * _CANNOT_ grow (all allocation attempts will fail).
 *
 *  == If there is any more data after the above declared one they have
 *     been allocated via. the "struct hack" method (search for more info). ==
 *
 *  Next, possibly, comes the reference count (of the given length[2]).
 *  Next, if not "", comes the length of the data (of the given length[2]).
 *  Next, if non-zero length, comes the data, which can include NIL bytes.
 *  And finally, if not "", a NIL byte, to make sure it's always a valid C-Style
 * string (although obviously any embeded NIL bytes will make it look shorter
 * if something else just uses strlen(ustr_cstr())).
 *
 * [2] The sizes can currently be 1, 2, 4 or 8 bytes. Depending on the mapping
 * of the value in the first byte (length changes dynamically, as you add data).
 *
 *  Examples:
 *
 *  The allocated string "a", using exact allocations and no reference
 * counting, is the 4 bytes:
 *
 *     {0x81, 0x01, 'a', 0x00}
 *
 *
 *  The allocated string "ab", using non-exact allocations and 1 byte reference
 * counting (shared by 13 users), is the 6 bytes (there is no waste due to
 * non-exact allocations):
 *
 *     {0xA5, 0x0D, 0x02, 'a', 'b', 0x00}
 *
 *
 *  The allocated string "abcdefghijklmnop" (16 bytes, and a NIL), with
 * non-exact allocations 2 byte reference counting (shared by 1,003 users), is
 * the 24 bytes (with 3 bytes of "unused" space at the end):
 *
 *     {0xA9, 0x03, 0xEB, 0x10, 'a', 'b', [...], 'o', 'p', 0x00, <x>, <x>, <x>}
 */

struct Ustr_pool {
	void *(*pool_sys_malloc)(struct Ustr_pool *, size_t);
	void *(*pool_sys_realloc)(struct Ustr_pool *, void *, size_t, size_t);
	void (*pool_sys_free)(struct Ustr_pool *, void *);

	struct Ustr_pool *(*pool_make_subpool)(struct Ustr_pool *);
	void (*pool_clear)(struct Ustr_pool *);
	void (*pool_free)(struct Ustr_pool *);
};

static inline size_t ustr__ref_del(struct Ustr *s1) {
//  USTR_ASSERT(ustr_assert_valid(s1));

	if (!ustr_alloc(s1))
		return (-1);

	switch (USTR__REF_LEN(s1)) {
	case 8:
	case 4:
	case 2:
	case 1: {
		size_t ref = ustr_xi__ref_get(s1);

		if (ref == 0)
			return (-1);
		if (ref == 1) /* Special case this so it doesn't "go shared"
		 * plus this is a common case and doing this is
		 * marginally faster */
			return (0);

		ustr__ref_set(s1, ref - 1);
		return (ref - 1);
	}

	case 0: /* Ustr with no reference count */

//		USTR_ASSERT_NO_SWITCH_DEF("Ref. length bad for ustr__ref_del()");
	}

	return (0);
}

extern inline size_t ustr_xi__ref_get(const struct Ustr *s1) {
	return (ustr_xi__embed_val_get(s1->data + 1, USTR__REF_LEN(s1)));
}
extern inline int ustr_shared(const struct Ustr *s1) {
	return (ustr_ro(s1) || (ustr_alloc(s1) && !ustr_xi__ref_get(s1)));
}

extern inline size_t ustr_len(const struct Ustr *s1) { /* NOTE: can't call ustr_assert_valid() here due to recursion,
 * also because assert_valid() is inline, and defined later on. */
//	USTR_ASSERT(s1);

	if (!s1->data[0])
		return (0);

	return (ustr_xi__embed_val_get(s1->data + 1 + USTR__REF_LEN(s1),
			USTR__LEN_LEN(s1)));
}

extern inline size_t ustr_xi__embed_val_get(const unsigned char *data,
		size_t len) {
	size_t ret = 0;

	switch (len) {
	case 0:
		return (-1);
	case 4:
		ret |= (((size_t) data[3]) << 24);
		ret |= (((size_t) data[2]) << 16);
	case 2:
		ret |= (((size_t) data[1]) << 8);
	case 1:
		ret |= (((size_t) data[0]) << 0);

//      USTR_ASSERT_NO_SWITCH_DEF("Val. length bad for ustr_xi__embed_val_get()");
	}

	return (ret);
}

/* these are compound bit tests */
extern inline int ustr_ro(const struct Ustr *s1) {
	return (!(s1->data[0] & USTR__BITS_RW));
}
extern inline int ustr_fixed(const struct Ustr *s1) {
	return ((s1->data[0] & USTR__BITS_RW) == USTR__BITS_FIXED);
}
extern inline int ustr_limited(const struct Ustr *s1) {
	return ((s1->data[0] & USTR__BITS_RWLIM) == USTR__BITS_LIMITED);
}

static inline int ustr_setf_enomem_err(struct Ustr *s1) {
//  USTR_ASSERT(ustr_assert_valid(s1));

	errno = USTR__ENOMEM;
	if (!ustr_owner(s1))
		return (USTR_FALSE);

	s1->data[0] |= USTR__BIT_ENOMEM;
	return (USTR_TRUE);
}

static inline int ustr_owner(const struct Ustr *s1) {
//  USTR_ASSERT(ustr_assert_valid(s1));

	if (ustr_ro(s1))
		return (USTR_FALSE);
	if (ustr_fixed(s1))
		return (USTR_TRUE);

	switch (USTR__REF_LEN(s1)) {
	case 4:
		if (s1->data[4])
			return (USTR_FALSE);
		if (s1->data[3])
			return (USTR_FALSE);
	case 2:
		if (s1->data[2])
			return (USTR_FALSE);

	case 1:
		return (s1->data[1] == 1);
	}

	return (USTR_TRUE); /* Ustr with no ref. count */
}

static inline int ustr_add_cstr(struct Ustr **ps1, const char *cstr) {
	return (ustr_add_buf(ps1, cstr, strlen(cstr)));
}

static inline int ustrp__add_buf(struct Ustr_pool *p, struct Ustr **ps1,
		const void *s2, size_t len) {
	if (!ustrp__add_undef(p, ps1, len))
		return (USTR_FALSE);

	ustr__memcpy(*ps1, ustr_len(*ps1) - len, s2, len);

	return (USTR_TRUE);
}
static inline int ustr_add_buf(struct Ustr **ps1, const void *s2, size_t len) {
	return (ustrp__add_buf(0, ps1, s2, len));
}

/* NOTE: This is one of the two main "allocation" functions --
 * this is one of three things funcs that gets more data via. REALLOC.
 * Others are in ustr-set.h */
static inline
int ustrp__add_undef(struct Ustr_pool *p, struct Ustr **ps1, size_t len) {
	struct Ustr *s1 = USTR_NULL;
	struct Ustr *ret = USTR_NULL;
	size_t clen = 0;
	size_t nlen = 0;
	size_t sz = 0;
	size_t oh = 0;
	size_t osz = 0;
	size_t nsz = 0;
	int alloc = USTR_FALSE;

//	USTR_ASSERT(ps1 && ustrp__assert_valid(!!p, *ps1));

	if (!len)
		return (USTR_TRUE);

	s1 = *ps1;
	clen = ustr_len(s1);
	if ((nlen = clen + len) < clen) /* overflow */
		goto fail_enomem;

	if (ustr__rw_mod(s1, nlen, &sz, &oh, &osz, &nsz, &alloc)) {
		size_t eos_len = sizeof(USTR_END_ALOCDx);

		if (alloc && !ustrp__rw_realloc(p, ps1, !!sz, osz, nsz))
			return (USTR_FALSE);
		ustr__terminate((*ps1)->data, ustr_alloc(*ps1), (oh - eos_len) + nlen);
		ustr__len_set(*ps1, nlen);

//		USTR_ASSERT(ustrp__assert_valid(!!p, *ps1));
		return (USTR_TRUE);
	}

	if (ustr_limited(s1)) {
		ustr_setf_enomem_err(s1);
		return (USTR_FALSE);
	}

	if (!(ret = ustrp__dupx_undef(p, USTR__DUPX_FROM(s1), nlen)))
		goto fail_enomem;

	ustr__memcpy(ret, 0, ustr_cstr(s1), ustr_len(s1));
	ustrp__sc_free2(p, ps1, ret);

//	USTR_ASSERT(ustrp__assert_valid(!!p, *ps1));
	return (USTR_TRUE);

	fail_enomem: ustr_setf_enomem_err(s1);
	return (USTR_FALSE);
}

/* Can we actually RW to this Ustr, at _this_ moment, _this_ len */
static inline
int ustr__rw_mod(struct Ustr *s1, size_t nlen, size_t *sz, size_t *oh,
		size_t *osz, size_t *nsz, int *alloc) {
	size_t lbytes = 0;
	size_t sbytes = 0;

	if (!ustr_owner(s1))
		return (USTR_FALSE);

	*sz = 0;
	if (ustr_sized(s1))
		*sz = ustr__sz_get(s1);
	*osz = *sz;

	lbytes = USTR__LEN_LEN(s1);
	if (*sz)
		sbytes = lbytes;
//  USTR_ASSERT(!*sz || (ustr__nb(*sz) == lbytes) ||
//              ((ustr__nb(*sz) == 1) && (lbytes == 2))); /* 2 is the minimum */

	if (ustr__nb(nlen) > lbytes)
		return (USTR_FALSE); /* in theory we could do better, but it's _hard_ */

	*oh = 1 + USTR__REF_LEN(s1) + lbytes + sbytes + sizeof(USTR_END_ALOCDx);
	*nsz = *oh + nlen;

	if (*nsz < nlen)
		return (USTR_FALSE);
	if (*nsz > USTR__SIZE_MAX) /* 32bit overflow on 64bit size_t */
		return (USTR_FALSE);

	*alloc = USTR_FALSE; /* don't need to allocate/deallocate anything */
	if (*nsz <= *sz)
		return (USTR_TRUE); /* ustr_sized() */

	if (!ustr_exact(s1))
		*nsz = ustr__ns(*nsz);

	*osz = ustr_size_alloc(s1);

	if (!*sz && (*nsz == *osz))
		return (USTR_TRUE);

	*alloc = ustr_alloc(s1); /* _do_   need to deallocate */
	if (!*sz && (*nsz <= *osz))
		return (USTR_TRUE);

	if (!*alloc)
		return (USTR_FALSE);

	return (USTR_TRUE);
}

static inline size_t ustr__sz_get(const struct Ustr *s1) {
	size_t lenn = 0;

//  USTR_ASSERT(!ustr_ro(s1));
//  USTR_ASSERT( ustr_sized(s1));

	lenn = USTR__LEN_LEN(s1);

	return (ustr_xi__embed_val_get(s1->data + 1 + USTR__REF_LEN(s1) + lenn,
			lenn));
}

static inline
int ustrp__rw_realloc(struct Ustr_pool *p, struct Ustr **ps1, int sized,
		size_t osz, size_t nsz) {
	struct Ustr *ret = USTR_NULL;

//	USTR_ASSERT(ustr_alloc(*ps1));
//	USTR_ASSERT(osz == ustr_size_alloc(*ps1));
//	USTR_ASSERT(sized == !!sized);
//	USTR_ASSERT(sized == ustr_sized(*ps1));
//	ustr_assert(USTR__ASSERT_MALLOC_CHECK_MEM(p, *ps1));

	/*  printf("1. p=%p, osz=%zu, nsz=%zu\n", p, osz, nsz); */
	if (p)
		ret = p->pool_sys_realloc(p, *ps1, osz, nsz);
	else
		ret = USTR_CONF_REALLOC(*ps1, nsz);

	if (!ret) {
		ustr_setf_enomem_err(*ps1);
		return (USTR_FALSE);
	}
	if (sized)
		ustr__sz_set(ret, nsz);

	*ps1 = ret;

	return (USTR_TRUE);
}

static inline void ustr__sz_set(struct Ustr *s1, size_t sz) { /* can't validate as this is called before anything is setup */
	size_t lenn = 0;

//  USTR_ASSERT(!ustr_ro(s1));
//  USTR_ASSERT(ustr_sized(s1));

	lenn = USTR__LEN_LEN(s1);

	ustr__embed_val_set(s1->data + 1 + USTR__REF_LEN(s1) + lenn, lenn, sz);
}

extern inline const char *ustr_cstr(const struct Ustr *s1) { /* NOTE: can't call ustr_assert_valid() here due to recursion,
 * also because assert_valid() is inline, and defined later on. */
	size_t lenn = 0;

//  USTR_ASSERT(s1);

	if (!s1->data[0])
		return ((const char *) s1->data);

	lenn = USTR__LEN_LEN(s1);
	if (ustr_sized(s1))
		lenn *= 2;

	return ((const char *) (s1->data + 1 + USTR__REF_LEN(s1) + lenn));
}

static inline void ustrp__free(struct Ustr_pool *p, struct Ustr *s1) {
	if (!s1)
		return;

//  USTR_ASSERT(ustrp__assert_valid(!!p, s1));

	if (!ustr__ref_del(s1)) {
		if (p)
			p->pool_sys_free(p, s1);
		else
			USTR_CONF_FREE(s1);
	}
}

static inline void ustr_free(struct Ustr *s1) {
	return (ustrp__free(0, s1));
}

#endif /* USTR_PRIVATE_H_ */
